#include <stdio.h>
#include <unistd.h>
#include "include/ui.hpp"
#include "include/database.hpp"

int main(int argc, char const *argv[]) {
  Database db = createDB();
  char error[255];

  createTable("PERSONA", db, error);
  addCol("PERSONA", "CI", INTEGER, db, error);
  addCol("PERSONA", "NOMBRE", STRING, db, error);
  addCol("PERSONA", "APELLIDO", STRING, db, error);
  createTable("DIRECCION", db, error);
  addCol("DIRECCION", "ID", INTEGER, db, error);
  addCol("DIRECCION", "CALLE", STRING, db, error);
  addCol("DIRECCION", "NUMERO", INTEGER, db, error);
  createTable("EMPRESA", db, error);
  addCol("EMPRESA", "NOMBRE", STRING, db, error);
  addCol("EMPRESA", "PERSONA", STRING, db, error);
  addCol("EMPRESA", "DIRECCION", INTEGER, db, error);

  insertInto("PERSONA", "2583694:Luis:Perez", db, error);
  insertInto("PERSONA", "1478526:Jose:Rodriguez", db, error);

  insertInto("DIRECCION", "1:Italia:4444", db, error);
  insertInto("DIRECCION", "2:Colonia:1999", db, error);
  insertInto("DIRECCION", "3:Asamblea:1245", db, error);

  insertInto("EMPRESA", "Anep:2583694:1", db, error);
  insertInto("EMPRESA", "Ancap:1478526:3", db, error);
  insertInto("EMPRESA", "Ancap:2583694:1", db, error);

  mainMenu(db);
  return 0;
}
