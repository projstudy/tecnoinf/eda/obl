#include "../include/column.hpp"
#include "../include/data.hpp"
#include "../include/utils.hpp"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

struct _column {
  char *name;
  _type type;
  Data dataList;
  Column next;
};

/**
 * pre:
 * post: verifica que no este duplicada la pk
 * */
bool existsPk(Column columnList, char const *value) {
  if(columnList == NULL){
    return false;
  }
  return columnList != NULL && existsValue(columnList->dataList, value);
};

/**
 * pre:
 * post: agrega nueva row
 * */
void insertRow(Column &columnList, ParsedData parsed) {
  int dataPos = 0;
  char *value = getParsedValue(parsed, dataPos);
  int index = addDataSorted(columnList->dataList, value, columnList->type);
  dataPos++;
  //Para todas la columnas apartir desde la segunda
  for (Column c = columnList->next; c != NULL; c = c->next) {
    value = getParsedValue(parsed, dataPos);
    addDataAtIndex(c->dataList, value, index);
    dataPos++;
  }
};


/**
 * Pre:
 * Post: elimina todas las columnas y sus datos
 */
void deleteAllColumns(Column &columnList) {
  if (columnList == NULL) {
    return;
  } else {
    // recursion sobre columnas
    deleteAllColumns(columnList->next);
    // eliminar los datos de la columna
    deleteAllData(columnList->dataList);
    // eliminar la columna
    delete[] columnList->name;
    delete columnList;
    columnList = NULL;
  }
};

/**
 * Pre:
 * Post: retorna una nueva columna
 */
Column createColumn(char const *name, _type type) {
  Column column = new _column;
  column->name = new char[strlen(name) + 1];
  strcpy(column->name, name);
  column->dataList = NULL;
  column->next = NULL;
  column->type = type;
  return column;
}

/**
 * Pre:
 * Post: retorna true si existe una columna con el mismo nombre
 */
Column getColumn(char const *name, Column column) {
  if (column == NULL) {
    return NULL;
  } else {
    if (strcmp(column->name, name) == 0) {
      return column;
    } else {
      return getColumn(name, column->next);
    }
  }
};

/**
 * Pre:
 * Post: Imprime el nombre de la columna
 * */
void printColumn(Column columnList) {
  printf("\t");
  for (Column c = columnList; c != NULL; c = c->next) {
    if (c->next == NULL) {
      printf("%s", c->name);
    } else {
      printf("%s:", c->name);
    }
  }
  printf("\n");
  int totalData = getDataCount(columnList->dataList);
  for (int index = 0; index < totalData; ++index) {
    printf("\t");
    for (Column c = columnList; c != NULL; c = c->next) {
      printData(c->dataList, index, c->next != NULL);
    }
    printf("\n");
  }
}


/**
 * Pre:
 * Post: Elimina una o multiples "row" de la tabla que cumplan la condicion;
 * */
void deleteRow(Column columnList, char const *searchCol, char const *comparator, char const *condition) {
  Column targetCol = getColumn(searchCol, columnList);
  //Si es la condicion es "" borra todo
  if (strcmp(condition, "") == 0) {
    for (Column c = columnList; c != NULL; c = c->next) {
      deleteAllData(c->dataList);
    }
  } else {
    /* Si la condicion es otra es diferente a ""
     * busco en la row cual es la posicion en la que se encuentra
     */
    ArrayIndex a = initArray(0);
    getDataIndex(targetCol->dataList, comparator, condition, targetCol->type, a);
    for (int j = 0; j < getArraySize(a); ++j) {
      for (Column c = columnList; c != NULL; c = c->next) {
        //Borrar posicion X - j
        //Xq despues de borrada una posicion, la lista se corre un lugar
        int posDelete= getArrayValue(a,j);
        deleteDataAt(c->dataList, posDelete - j);
      }
    }
    //Eliminar el array
    freeArray(a);
  }
}


/**
 * Pre:
 * Post: Elimina una o multiples "row" de la tabla que cumplan la condicion;
 * */
void updateRow(Column &columnList, char const *searchColumn, char const *columnToMod, char const *comparator,
               char const *searchValue, char const *newValue) {
  Column searchCol = getColumn(searchColumn, columnList);
  Column modCol = getColumn(columnToMod, columnList);
  ArrayIndex dataIndexes = initArray(0);
  //Busco todos los posibls indices a modificar y los almaceno en el array dataIndexes
  getDataIndex(searchCol->dataList, comparator, searchValue, searchCol->type, dataIndexes);
  for (int i = 0; i < getArraySize(dataIndexes); ++i) {
    //Por cada index en en dataIndex, actualizo la columna con el nuevo valor
    updateDataAt(modCol->dataList, getArrayValue(dataIndexes, i), newValue);
  }
  freeArray(dataIndexes);
}

/**
 * Pre:
 * Post: Retorna true si existe una columna con el mismo nombre
 * */
bool columnExists(Column columnList, char const *columnName) {
  if (columnList == NULL) {
    return false;
  } else {
    if (strcmp(columnList->name, columnName) == 0) {
      return true;
    } else {
      return columnExists(columnList->next,columnName);
    }
  }
}

/**
 * Pre:
 * Post: Retorna true si la columna tiene datos almacenados
 * */
bool columnHasData(Column columnList) {
  return columnList != NULL && columnList->dataList != NULL;
}

/**
 * Pre:
 * Post: Agrega una nuneva columna al final de la lista de columnas.
 * */
void addColumn(Column &columnList, char const *columnName, _type type) {
  Column newCol = createColumn(columnName, type);
  Column prev = NULL;
  for (Column current = columnList; current != NULL; current = current->next) {
    prev = current;
  }
  if (prev != NULL) {
    prev->next = newCol;
  } else {
    columnList = newCol;
  }
}

/**
 * Pre:
 * Post: Elimina una coluna y todos sus datos
 * */
void deleteColumn(Column &columnList, char const *columnName) {
  Column head = columnList;
  Column prev = NULL;
  while (strcmp(columnName, head->name) != 0) {
    prev = head;
    head = head->next;
  }
  //Elimino todos sus datos antes de borrar la propia col;
  deleteAllData(head->dataList);
  if (prev == NULL) {
    columnList = head->next;
  } else {
    prev->next = head->next;
  }
  delete[] head->name;
  delete head;
}

/**
 * Pre:
 * Post: retorna true si la columnName es la primer columna (pk)
 * */
bool isColumnPk(Column columnList, char const *columnName) {
  return strcmp(columnList->name, columnName) == 0;
}

/**
 * Pre:
 * Post: retorna true si el tipo de cada dato parseado, es igual al tipo de la columna que le
 * corresponde
 * */
bool typesMatchColumns(Column columnList, ParsedData parsed) {
  bool result = true;
  int columnPos = 0;
  for (Column c = columnList; c != NULL; c = c->next) {
    if (c->type == INTEGER) {
      result = result && isInt(parsed, columnPos);
    }
    columnPos++;
  };
  return result;
}

/**
 * Pre:
 * Post: retorna true si el tipo de la condicion es igual al tipo de la columnTarget
 * */
bool typeMatches(Column columnList, char const *columnTarget, char const *condition) {
  bool result = true;
  Column c = getColumn(columnTarget, columnList);
  if (c != NULL) {
    //Solo check de int
    if (c->type == INTEGER) {
      for (size_t i = 0; i < strlen(condition); ++i) {
        result = isdigit(*condition) && result;
        condition++;
      }
    }
  }
  return result;
}
