#include "../include/database.hpp"
#include "../include/table.hpp"
#include "../include/utils.hpp"
#include <stdio.h>
#include <string.h>

struct _database {
  Table tableList;
};

/**
 * PRE: el nombre no puede existir en la base
 * POST: crea una nueva tabla con el nombre pasado por parametro
 *u */
TipoRet createTable(const char *nombreTabla, Database &db, char *msg) {
    //Si la tabla no existe
    if (getTable(db->tableList, nombreTabla) == NULL) {
      addTable(db->tableList, nombreTabla);
      return OK;
    } else {
      //Ya existe la tabla
      strcpy(msg, "Ya existe la tabla ");
      strcat(msg, nombreTabla);
      return ERROR;
    }
}

/**
 * PRE: la tabla nombreTabla debe existir
 * POST: elimina la tabla indicada incluida sus columnas y datos
 * */
TipoRet dropTable(char const *nombreTabla, Database &db, char *msg) {
  if (!isDbEmpty(db)) {
    Table table = getTable(db->tableList, nombreTabla);
    if (table != NULL) {
      deleteTable(db->tableList, nombreTabla);
      return OK;
    } else {
      strcpy(msg, "No existe la tabla ");
      strcat(msg, nombreTabla);
      return ERROR;
    }
  } else {
    // DB empty
    strcpy(msg, "La DB esta vacia.");
    return ERROR;
  }
}

/**
 * PRE: la tabla nombreTabla debe existir, la columna no debe existir en la
 * tabla POST: agrega a la tabla nombreTabla la columna nombreCol
 * */
TipoRet addCol(char const *nombreTabla, char const *nombreCol, _type type, Database &db, char *msg) {
  TipoRet result;
  if (!isDbEmpty(db)) {
    Table table = getTable(db->tableList, nombreTabla);
    if (table != NULL) {
      //Si la tabla no tiene datos..
      if (!tableHasData(table)) {
        //Si la tabla no tiene la columna...
        if (!tableHasColumn(table, nombreCol)) {
          addColumnToTable(table, nombreCol, type);
          return OK;
        } else {
          //Ya existen datos ingresados, no se puede crear la columna.
          result = ERROR;
          // Error, tabla no existente
          strcpy(msg, "Ya existe la columna ");
          strcat(msg, nombreCol);
        }
      } else {
        // Error, columna duplicada
        result = ERROR;
        strcpy(msg, "La tabla ya cuenta con datos, no se puede crear un nueva columna");
      }
    } else {
      result = ERROR;
      // Error, tabla no existente
      strcpy(msg, "No existe la tabla ");
      strcat(msg, nombreTabla);
    }
  } else {
    strcpy(msg, "La DB esta vacia.");
    result = ERROR;
  }
  return result;
}

/**
 * PRE:
 * POST:
 * */
TipoRet dropCol(char const *nombreTabla, char const *nombreCol, Database &db, char *msg) {
  TipoRet result;
  if (!isDbEmpty(db)) {
    Table table = getTable(db->tableList, nombreTabla);
    if (table != NULL) {
      if (tableHasColumn(table, nombreCol)) {
        //Si la columna no es PK, no hay problema
        if (!isTablePkColumn(table, nombreCol)) {
          deleteColumFromTable(table, nombreCol);
          result = OK;
        } else {
          //SI es pk, tengo que verificar que sea la unica columna
          if (canTablePkColumnBeDeleted(table)) {
            deleteColumFromTable(table, nombreCol);
            result = OK;
          } else {
            strcpy(msg, "No se puede eliminar la columna porque es PK");
            result = ERROR;
          }
        }
      } else {
        strcpy(msg, "No existe la columna ");
        strcat(msg, nombreCol);
        result = ERROR;
      }
    } else {
      // Error, tabla no existente
      result = ERROR;
      strcpy(msg, "No existe la tabla ");
      strcat(msg, nombreTabla);
    }
  } else {
    strcpy(msg, "La DB esta vacia.");
    result = ERROR;
  }
  return result;
}

TipoRet insertInto(char const *nombreTabla, char const *valoresTupla, Database &db, char *msg) {
  TipoRet result;
  if (!isDbEmpty(db)) {
    if (isStringValid(valoresTupla)) {
      Table table = getTable(db->tableList, nombreTabla);
      if (table != NULL) {
        int qtyColumns = getColumnsQty(table);
        if (qtyColumns > 0) {
          ParsedData parsed = parseInsert(valoresTupla, qtyColumns);
          //fueron todos los datos necesarios ingresados?
          if (areAllColumnsProvided(parsed, qtyColumns)) {
            //types ok?
            if (typesMatchTable(table, parsed)) {
              //La pk siempre es la columna "0"
              char *pk = getParsedValue(parsed, 0);
              if (!isPkTaken(table, pk)) {
                insertIntoTable(table, parsed);
                result = OK;
              } else {
                strcpy(msg, "La pk se encuentra duplicada");
                result = ERROR;
              }
            } else {
              strcpy(msg, "Los datos no coinciden con el tipo necesario por las columnas.");
              result = ERROR;
            };
          } else {
            strcpy(msg, "Se deben ingresar los datos para todas las columnas");
            result = ERROR;
          }
          deleteParsedData(parsed, qtyColumns);
        } else {
          strcpy(msg, "La tabla no cuenta con columnas para que se puedan ingresar los datos.");
          result = ERROR;
        }
      } else {
        strcpy(msg, "No existe la tabla ");
        strcat(msg, nombreTabla);
        result = ERROR;
      }
    } else {
      //caracteres no aceptads
      strcpy(msg, "Caracteres no validos.");
      result = ERROR;
    }
  } else {
    strcpy(msg, "La DB esta vacia.");
    result = ERROR;
  }
  return result;
}

TipoRet deleteFrom(char const *nombreTabla, char const *condicionEliminar, Database &db, char *msg) {
  TipoRet result;
  if (!isDbEmpty(db)) {
    Table table = getTable(db->tableList, nombreTabla);
    if (table != NULL) {
      ParsedData parsed = parseUpdateOrDelete(condicionEliminar);
      char const *targetColumn = getParsedValue(parsed, 0);
      char const *condition = getParsedValue(parsed, 1);
      char const *comparator = getParsedValue(parsed, 2);
      if (tableHasColumn(table, targetColumn)) {
        if (typeMatchesColumn(table, targetColumn, condition)) {
          deleteTableRow(table, targetColumn, comparator, condition);
          result = OK;
        } else {
          strcpy(msg, "Los datos no coinciden con el tipo necesario por las columnas.");
          result = ERROR;
        }
      } else {
        strcpy(msg, "No existe la columna ");
        strcat(msg, targetColumn);
        result = ERROR;
      }
      deleteParsedData(parsed, 3);
    } else {
      strcpy(msg, "No existe la tabla ");
      strcat(msg, nombreTabla);
      result = ERROR;
    }
  } else {
    strcpy(msg, "La DB esta vacia.");
    result = ERROR;
  }
  return result;
}

TipoRet update(char const *nombreTabla, char const *condicionModificar,
               char const *columnaModificar, char const *valorModificar, Database &db, char *msg) {
  TipoRet result;
  if (!isDbEmpty(db)) {
    Table table = getTable(db->tableList, nombreTabla);
    if (table != NULL) {
      if (tableHasColumn(table, columnaModificar)) {
        ParsedData parsed = parseUpdateOrDelete(condicionModificar);
        char const *targetColumn = getParsedValue(parsed, 0);
        char const *comparator = getParsedValue(parsed, 2);
        char const *condition = getParsedValue(parsed, 1);
        if (tableHasColumn(table, targetColumn)) {
          if (typeMatchesColumn(table, targetColumn, condition)) {
            if (typeMatchesColumn(table, columnaModificar, valorModificar)) {
              //Si no es la pk de la tabla...
              if (!isTablePkColumn(table, columnaModificar)) {
                updateTableRows(table, targetColumn, comparator, condition, columnaModificar, valorModificar);
                result = OK;
              } else {
                strcpy(msg, "Las pk no se pueden actualizar.");
                result = ERROR;
              }
            } else {
              strcpy(msg, "Los datos no coinciden con el tipo necesario por las columnas.");
              result = ERROR;
            }
          } else {
            strcpy(msg, "Los datos no coinciden con el tipo necesario por las columnas.");
            result = ERROR;
          }
        } else {
          //La columna de la condicion no existe
          strcpy(msg, "No existe la columna ");
          strcat(msg, targetColumn);
          result = ERROR;
        }
        deleteParsedData(parsed, 3);
      } else {
        strcpy(msg, "No existe la columna ");
        strcat(msg, columnaModificar);
        result = ERROR;
      }
    } else {
      strcpy(msg, "No existe la tabla ");
      strcat(msg, nombreTabla);
      result = ERROR;
    }
  } else {
    strcpy(msg, "La DB esta vacia.");
    result = ERROR;
  }
  return result;
}

TipoRet printDataTable(char const *nombreTabla, Database db, char *msg) {
  TipoRet result;
  if (!isDbEmpty(db)) {
    Table table = getTable(db->tableList, nombreTabla);
    if (table != NULL) {
      printTable(table);
      result = OK;
    } else {
      strcpy(msg, "No existe la tabla ");
      strcat(msg, nombreTabla);
      result = ERROR;
    }
  } else {
    strcpy(msg, "La DB esta vacia.");
    result = ERROR;
  }
  return result;
}

/**
 * Pre:
 * Post: retorna una nueva database
 * */
Database createDB() {
  Database db = new _database;
  db->tableList = NULL;
  return db;
};

/**
 * Pre:
 * Post: retorna una nueva database
 * */
void dropDB(Database &db) {
  delete db;
};

/**
 * Pre:
 * Post: retorna true si una database esta vacia;
 * */
bool isDbEmpty(Database db) { return db == NULL || db->tableList == NULL; };

