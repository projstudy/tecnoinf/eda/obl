#include <cstdio>
#include "../include/ui.hpp"
#include "../include/column.hpp"

void mainMenu(Database &db) {
  clearScreen();
  char option;
  do {
    printf("\n");
    printf("\n");
    printf("\t====SELECCIONE UNA OPCION====\t\n");
    printf("\n");
    printf("\t1- CREAR TABLA\t\n");
    printf("\t2- ELIMINAR TABLA\t\n");
    printf("\t3- AGREGAR COLUMNA\t\n");
    printf("\t4- ELIMINAR COLUMNA\t\n");
    printf("\t5- INSERTAR TUPLA\t\n");
    printf("\t6- ELIMINAR TUPLA\t\n");
    printf("\t7- MODIFICAR TUPLA\t\n");
    printf("\t8- LISTAR TABLA\t\n");
    printf("\t9- ELIMINAR DB Y SALIR\t\n");
    printf(YEL "\t-> " RESET);
    scanf(" %c", &option);
    switch (option) {
      case '1':
        clearScreen();
        getDataCreateTable(db);
        break;
      case '2':
        clearScreen();
        getDataDropTable(db);
        break;
      case '3':
        clearScreen();
        getDataAddColumn(db);
        break;
      case '4':
        clearScreen();
        getDataDropColumn(db);
        break;
      case '5':
        clearScreen();
        getDataInsertTupla(db);
        break;
      case '6':
        clearScreen();
        getDataDeleteTupla(db);
        break;
      case '7':
        clearScreen();
        getDataUpdate(db);
        break;
      case '8':
        clearScreen();
        getDataPrintTable(db);
        break;
      case '9':
        printf(RED "\tDB Eliminada." RESET " Presione ENTER para continuar\n");
        pressEnter();
        dropDB(db);
        break;
      default: {
        printf("\tOpcion invalida. Presione ENTER para continuar\n");
        pressEnter();
        break;
      };
    }
    clearScreen();
  } while (option != '9');


}

/**
 * Pre:
 * Post: Limpia la pantalla
 */
void clearScreen() {
  //Caracteres especiale: ANSI control sequences
  printf("\x1b[H\x1b[J");
}

void pressEnter() {
  while (getchar() != '\n'); // option TWO to clean stdin
  getchar(); // wait for ENTER
}

/**
 * Pre:
 * Post: Valida el nombre de la tabla a crear, si no existe la crea.
 */
void getDataCreateTable(Database &db) {
  char nombreTabla[255];
  printf("\tIngrese el nombreTabla de la tabla\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  char msg[255];
  TipoRet result = createTable(nombreTabla, db, msg);
  if (result == ERROR) {
    printf(RED "\n\tSe ha producido un error: %s\n\t" RESET "Presione ENTER para continuar.", msg);
    pressEnter();
  } else {
    printf("\n\tLa tabla %s ha sido creada.\n\tPresione ENTER para continuar", nombreTabla);
    pressEnter();
  }
}

/**
* Pre:
* Post: Valida el nombre de la tabla a eliminar, si existe la elimina.
*/
void getDataDropTable(Database &db) {
  char nombreTabla[255];
  printf("\tIngrese el nombre de la tabla a ELIMINAR\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  char msg[255];
  TipoRet result = dropTable(nombreTabla, db, msg);
  if (result == ERROR) {
    printf(RED "\n\tSe ha producido un error: %s" RESET "\n\tPresione ENTER para continuar.", msg);
    pressEnter();
  } else {
    printf("\n\tLa tabla %s ha sido eliminada.\n\tPresione ENTER para continuar", nombreTabla);
    pressEnter();
  }
}

/**
* Pre:
* Post: Inserta columna en una tabla.
*/
void getDataAddColumn(Database &db) {
  char nombreTabla[255];
  char nombreCol[255];
  char msg[255];
  char opt;
  _type type;
  printf("\tIngrese el nombre de la tabla donde insertara la nueva columna\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  printf("\tIngrese el nombre de la columna que insertara en la tabla %s\n \t" BLINK YEL "-> " RESET, nombreTabla);
  scanf("%s", nombreCol);
  do {
    printf("\tQue tipo de columna va a insertar\n\t1-Enteros\n\t2-Caracteres\n\t" BLINK YEL "-> " RESET);
    scanf(" %c", &opt);
    switch (opt) {
      case '1': {
        type = INTEGER;
        break;
      }
      case '2': {
        type = STRING;
        break;
      }
      default:
        printf(RED "\tHa ingresado una opcion incorrecta, verifique.\n" RESET);
    }
  } while (opt != '1' && opt != '2');

  printf("\tUsted va a insertar la columna %s en la tabla %s - Esta de acuerdo? S/N ", nombreCol, nombreTabla);
  printf(YEL BLINK "\n\t ->" RESET);
  scanf(" %c", &opt);
  if (opt == 's' || opt == 'S') {
    TipoRet result = addCol(nombreTabla, nombreCol, type, db, msg);
    if (result == ERROR) {
      printf(RED "\n\tSe ha producido un error: %s" RESET "\n\tPresione ENTER para continuar.", msg);
      pressEnter();
    } else {
      printf("\n\tLa columna %s ha sido creada.\n\tPresione ENTER para continuar", nombreCol);
      pressEnter();
    }
  }
}


/**
* Pre:
* Post: Elimina una columna de una tabla.
*/
void getDataDropColumn(Database &db) {
  char nombreTabla[255];
  char nombreCol[255];
  char msg[255];
  char opcion = 'x';
  // _type type;
  printf("\tIngrese el nombre de la tabla donde esta la columna a eliminar\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  printf("\tIngrese el nombre de la columna a eliminar %s\n \t" BLINK YEL "-> " RESET, nombreTabla);
  scanf("%s", nombreCol);
  printf("\tUsted va a eliminar la columna %s de la tabla %s - Esta de acuerdo? S/N", nombreCol, nombreTabla);
  printf(YEL BLINK"\n\t ->" RESET);
  scanf(" %c", &opcion);
  if (opcion == 's' || opcion == 'S') {
    TipoRet result = dropCol(nombreTabla, nombreCol, db, msg);
    if (result == ERROR) {
      printf(RED "\n\tSe ha producido un error: %s" RESET "\n\tPresione ENTER para continuar.", msg);
      pressEnter();
    } else {
      printf("\n\tLa columna %s ha sido eliminada.\n\tPresione ENTER para continuar", nombreCol);
      pressEnter();
    }
  }
}


/**
* Pre:
* Post: inserta una tupla en una tabla.
*/
void getDataInsertTupla(Database &db) {
  char nombreTabla[255];
  char datosTupla[255];
  char msg[255];
  printf("\tIngrese la tabla donde va a insertar\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  printf(
      "\tIngrese los datos a insertar en la tabla de la siguiente forma < datoCol1:datoCol2> \n \t" BLINK YEL "-> " RESET);
  scanf("%s", datosTupla);
  TipoRet result = insertInto(nombreTabla, datosTupla, db, msg);
  if (result == ERROR) {
    printf(RED "\n\tSe ha producido un error: %s" RESET "\n\tPresione ENTER para continuar.", msg);
    pressEnter();
  } else {
    printf("\n\tSe ha insertado la tupla %s en la tabla %s.\n\tPresione ENTER para continuar", datosTupla, nombreTabla);
    pressEnter();
  }
}

/**
 * Pre:
 * Post:Elimina una o multiples tuplas que cumplan la condicion ingresada de una tabla
 */
void getDataDeleteTupla(Database &db) {
  char nombreTabla[255];
  char condicionEliminar[255];
  char msg[255];
  printf("\tIngrese la tabla donde va a eliminar la tupla\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  printf("\tIngrese condicion a eliminar de la siguiente forma <nombreCol=dato> \n \t" BLINK YEL "-> " RESET);
  scanf("%s", condicionEliminar);
  TipoRet result = deleteFrom(nombreTabla, condicionEliminar, db, msg);
  if (result == ERROR) {
    printf(RED "\n\tSe ha producido un error: %s" RESET "\n\tPresione ENTER para continuar.", msg);
    pressEnter();
  } else {
    printf("\n\t La tabla %s ha sido modificada, los datos %s han sido eliminados.\n\tPresione ENTER para continuar",
           nombreTabla, condicionEliminar);
    pressEnter();
  }
}

/**
 * Pre:
 * Post: modificara los datos ingresados en la tabla especificada.
 */
void getDataUpdate(Database &db) {
  char nombreTabla[255];
  char condicionUpdate[255];
  char columnUpdate[255];
  char valorUpdate[255];
  char msg[255];
  printf("\tIngrese la tabla donde va a modificar la/s tupla/s\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  printf(
      "\tIngrese condicion para modificar la tupla de la siguiente forma <nombreCol=dato> \n \t" BLINK YEL "-> " RESET);
  scanf("%s", condicionUpdate);
  printf("\t Ingrese la columna a modificar\n \t" BLINK YEL "-> " RESET);
  scanf("%s", columnUpdate);
  printf("\t Ingrese el nuevo valor de la tupla\n \t" BLINK YEL "-> " RESET);
  scanf("%s", valorUpdate);
  TipoRet result = update(nombreTabla, condicionUpdate, columnUpdate, valorUpdate, db, msg);
  if (result == ERROR) {
    printf(RED "\n\tSe ha producido un error: %s" RESET "\n\tPresione ENTER para continuar.", msg);
    pressEnter();
  } else {
    printf("\n\t La tabla %s ha sido modificada, los datos %s han sido actualizados.\n\tPresione ENTER para continuar",
           nombreTabla, valorUpdate);
    pressEnter();
  }

}

void getDataPrintTable(Database &db) {
  char nombreTabla[255];
  char msg[255];
  printf("\tIngrese la tabla que desea visualizar\n \t" BLINK YEL "-> " RESET);
  scanf("%s", nombreTabla);
  TipoRet result = printDataTable(nombreTabla, db, msg);
  if (result == ERROR) {
    printf(RED "\n\tSe ha producido un error: %s" RESET, msg);
  }
  printf("\n\tPresione ENTER para continuar.");
  pressEnter();
}
