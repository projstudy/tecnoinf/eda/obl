#include "../include/data.hpp"
#include "../include/utils.hpp"
#include "../include/column.hpp"
#include <stdio.h>
#include <cstring>
#include <cstdlib>

struct _data {
  char *value;
  _data *next;
};

/**
 * Pre:
 * Post: retorna la posicion en la que se encuentra el dato que cumple la condicion
 *       retorna -1 en caso de que no se encuentre ningun dato
 * */
void getDataIndex(Data listData, char const *comparator, char const *searchValueStr, _type type, ArrayIndex &a) {
  int listIndex = 0;
  //dato a buscar convertido a INT
  long searchValueInt = strtol(searchValueStr, NULL, 10);
  //comparador
  char symbol = comparator[0];
  for (Data d = listData; d != NULL; d = d->next) {
    if (type == INTEGER) {
      //Si el dato que voy a comprobar es INT, convierto al dato almacenado a INT
      long storedValue = strtol(d->value, NULL, 10);
      switch (symbol) {
        case '>' : {
          if (storedValue > searchValueInt) {
            insertArray(a, listIndex);
          };
          break;
        };
        case '<' : {

          if (storedValue < searchValueInt) {
            insertArray(a, listIndex);
          };
          break;
        };
        case '=' : {
          if (storedValue == searchValueInt) {
            insertArray(a, listIndex);
          };
          break;
        };
        case '!' : {
          if (storedValue != searchValueInt) {
            insertArray(a, listIndex);
          };
          break;
        };
        case '*' : {
          //Si se utiliza el * no uso la version INT
          if (startsWith(d->value, searchValueStr)) {
            insertArray(a, listIndex);
          }
          break;
        };
        default: {
          break;
        }
      }
    } else {
      switch (symbol) {
        case '>' : {
          int i = strcmp(d->value, searchValueStr);
          if (i > 0) {
            insertArray(a, listIndex);
          }
          break;
        };
        case '<' : {
          int i = strcmp(d->value, searchValueStr);
          if (i < 0) {
            insertArray(a, listIndex);
          }
          break;
        };
        case '=' : {
          if (strcmp(d->value, searchValueStr) == 0) {
            insertArray(a, listIndex);
          }
          break;
        };
        case '!' : {
          if (strcmp(d->value, searchValueStr) != 0) {
            insertArray(a, listIndex);
          }
          break;
        };
        case '*' : {
          if (startsWith(d->value, searchValueStr)) {
            insertArray(a, listIndex);
          }
          break;
        };
        default: {
        }
      }
    }
    listIndex++;
  }
}

/**
 * Pre:
 * Post: Borra un dato en la posicion indicada (index);
 * */
void deleteDataAt(Data &data, int index) {
  Data head = data;
  Data prev = NULL;
  while (index > 0 || head == NULL) {
    index--;
    prev = head;
    head = head->next;
  }
  if (prev == NULL) {
    data = data->next;
    delete head;
  } else {
    prev->next = head->next;
    delete[] head->value;
    delete head;
  }
}

/**
 * Pre:
 * Post: actualiza dato en la posicion index de la lista
 * */
void updateDataAt(Data &data, int index, char const *newValue) {
  Data head = data;
  while (index > 0 || head == NULL) {
    index--;
    head = head->next;
  }
  delete[] head->value;
  head->value = new char[strlen(newValue) + 1];
  strcpy(head->value, newValue);
};

/**
 * Pre:
 * Post: Elimina todos los datos de una lista de datos
 * */
void deleteAllData(Data &data) {
  if (data == NULL) {
    return;
  } else {
    // Recursion hasta null;
    deleteAllData(data->next);
    // Eliminar el dato
    delete[] data->value;
    delete data;
    //Dejo dataList como null, porque la columna sigue apuntando a esta direccion.
    data = NULL;
  }
}

/**
 * pre:
 * post: Agrega un nuevo dato en el index especificado
 * */
void addDataAtIndex(Data &data, char const *value, int index) {
  Data prev = NULL;
  Data head = data;
  Data newNode = new _data;

  newNode->next = NULL;
  newNode->value = new char[strlen(value) + 1];
  strcpy(newNode->value, value);

  while (head != NULL && index > 0) {
    prev = head;
    head = head->next;
    index--;
  }
  if (prev == NULL) {
    //si es el primero de la lista (pero existen mas)
    if (head != NULL) {
      newNode->next = head;
    }
    //si es el unico de la lista
    data = newNode;
  } else {
    //Si va al medio o final
    newNode->next = head;
    prev->next = newNode;
  }
}


/**
 * Pre:
 * Post: Ingresa un dato en forma ordenada, y retorna en que posicion fue ingresado.
 * */
int addDataSorted(Data &data, char const *value, _type type) {
  Data newNode = new _data;
  newNode->value = new char[strlen(value) + 1];
  strcpy(newNode->value, value);
  int index = 0;
  Data current = data;

  if (type == INTEGER) {
    //Caso especia, es el primer dato porque la lista es vacia, o porque es menor al primero
    long newValue = strtol(value,NULL,10);
    //Posicion 0
    if (current == NULL || strtol(current->value,NULL,10) > newValue) {
      newNode->next = current;
      data = newNode;
    } else {
      index++;
      //Posicion 1 en adelante
      //Busco hasta encontrar uno que sea mayor o hata llegar al final
      while (current->next != NULL &&
             strtol(current->next->value, NULL, 10) < newValue) {
        current = current->next;
        index++;
      }
      newNode->next = current->next;
      current->next = newNode;
    }
  } else {
    /**
     * Mismo codigo, pero sin tratar los chars como numeros
     * */
    if (data == NULL || strcmp(data->value, newNode->value) > 0) {
      newNode->next = data;
      data = newNode;
    } else {
      /* Locate the node before the point of insertion */
      while (current->next != NULL &&
             strcmp(current->next->value, newNode->value) < 0) {
        current = current->next;
        index++;
      }
      newNode->next = current->next;
      current->next = newNode;
    }
  }
  return index;
}

/**
 * Pre:
 * Post: retorna true si un dato con el mismo valor ya se encuentra ingresado.
 * */
bool existsValue(Data dataList, char const *value) {
  if (dataList == NULL) {
    return false;
  } else {
    if (strcmp(dataList->value, value) == 0) {
      return true;
    } else {
      return existsValue(dataList->next, value);
    }
  }
}

void printData(Data data, int index, bool includeColon) {
  Data head = data;
  for (int i = 0; i < index; ++i) {
    head = head->next;
  }
  if (head != NULL) {
    if (includeColon) {
      printf("%s:", head->value);
    } else {
      printf("%s", head->value);
    }
  }
}

int getDataCount(Data data) {
  if (data == NULL) {
    return 0;
  }
  return getDataCount(data->next) + 1;
}
