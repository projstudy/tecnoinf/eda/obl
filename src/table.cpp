#include "../include/column.hpp"
#include "../include/database.hpp"
#include "../include/table.hpp"
#include "../include/utils.hpp"
#include <stdio.h>
#include <string.h>

struct _table {
  char *name;
  int qtyCol;
  Column columnList;
  Table next;
};

/**
* pre:
* post: retorna true si la lista de columnas solo tiene la pk
* */
bool canTablePkColumnBeDeleted(Table table){
  return table->qtyCol == 1;
}

/**
 * Pre:
 * Post: retorna true si la tabla cuenta con una columna con un nombre especificado
 * */
bool tableHasColumn(Table table, char const *columnName) {
  return columnExists(table->columnList, columnName);
}

/**
 * Pre:
 * Post: retorna nueva tabla
 * */
Table createTable(char const *name) {
  Table t = new _table;
  t->name = new char[strlen(name) + 1];
  strcpy(t->name, name);
  t->qtyCol = 0;
  t->columnList = NULL;
  t->next = NULL;
  return t;
};

/**
 * Pre:
 * Post: Elimina la tabla y todas sus columnas (y datos).
 * */
void deleteTable(Table &tableList, char const *name) {
  Table head = tableList;
  Table prev = NULL;
  // Mientras las strings no sean iguales, sigue buscando
  while (strcmp(name, head->name) != 0) {
    prev = head;
    head = head->next;
  }
  // Si es el primero
  if (prev == NULL) {
    tableList = head->next;
    deleteAllColumns(head->columnList);
  } else {
    // Si esta en el medio o final
    prev->next = head->next;
    deleteAllColumns(head->columnList);
  }
  delete[] head->name;
  delete head;
};

/**
 * Pre:
 * Post: retorna una tabla si la encuentra por su nombre. Caso contrario, retorna NULL
 * */
Table getTable(Table tableList, char const *name) {
  if (!isTableEmpty(tableList)) {
    if (strcmp(tableList->name, name) == 0) {
      return tableList;
    } else {
      return getTable(tableList->next, name);
    }
  } else {
    return NULL;
  }
};
/**
 * Pre:
 * Post: true si la tabla es vacia
 * */
bool isTableEmpty(Table table) {
  return table == NULL;
};

/**
 * Pre:
 * Post: Agrega una nueva tabla a la lista de tablas.
 * */
void addTable(Table &tableList, char const *name) {
  Table newTable = createTable(name);
  if (!isTableEmpty(tableList)) {
    newTable->next = tableList;
  }
  tableList = newTable;
};

/**
 * Pre:
 * Post: Retorna la cantidad de columnas que tiene una tabla
 * */
int getColumnsQty(Table table) {
  return table->qtyCol;
}

/**
 * Pre:
 * Post: Imprime el nombre de una tabla y sus columnas
 * */
void printTable(Table table) {
  if (!isTableEmpty(table) and table->columnList != NULL) {
    printf("\t%s\n", table->name);
    printColumn(table->columnList);
  } else {
    printf("\tNo hay tuplas en la tabla %s\n", table->name);
  }
};


/**
 * Pre:
 * Post: retorna true si una tabla tiene datos ingresados
 * */
bool tableHasData(Table table) {
  return columnHasData(table->columnList);
}

/**
 * Pre:
 * Post: agrega una columna a la tabla.
 * */
void addColumnToTable(Table &table, char const *nombreCol, _type type) {
  addColumn(table->columnList, nombreCol, type);
  table->qtyCol++;
}

/**
 * Pre: no verifica si una tabla tiene datos
 * Post: check si una columna puede ser eliminada (no es pk)
 * */
bool isTablePkColumn(Table table, char const *nombreCol) {
  return isColumnPk(table->columnList, nombreCol);
}

/**
 * Pre:
 * Post: Elimina una columna de la tabla
 * */
void deleteColumFromTable(Table &table, char const *columnName) {
  deleteColumn(table->columnList, columnName);
  table->qtyCol--;
}

/**
 * Pre:
 * Post: Comprueba si los datos parseados son los mismos que los de la tabla
 * */
bool typesMatchTable(Table table, ParsedData parsed) {
  return typesMatchColumns(table->columnList, parsed);
}

/**
 * Pre:
 * Post: Comprueba si la pk no se encuentra en uso en la tabla.
 * */
bool isPkTaken(Table table, char const *pk) {
  return existsPk(table->columnList, pk);
};

/**
 * Pre:
 * Post: Agrega una nueva row a la tabla
 * */
void insertIntoTable(Table &table, ParsedData parsed) {
  insertRow(table->columnList, parsed);
}

/**
 * Pre:
 * Post: Retorna true si un tipo de dato coincide con una columna en particular
 *      Usado para el deleteFrom o updateFrom donde un solo tipo es especificado
 * */
bool typeMatchesColumn(Table table, char const *targetColumn, char const *condition) {
  return typeMatches(table->columnList, targetColumn, condition);
}

/**
 * Pre:
 * Post: elimina una row de la tabla.
 * */
void deleteTableRow(Table &table, char const *targetColumn, char const *comparator, char const *condition) {
  deleteRow(table->columnList, targetColumn, comparator, condition);
}

void updateTableRows(Table &table, char const *targetColumn, char const *comparator, char const *condition,
                     char const *columnaModificar, char const *valorModificar){

  updateRow(table->columnList, targetColumn, columnaModificar, comparator, condition, valorModificar);
}

