#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "../include/utils.hpp"

struct _parsedData {
  char *data;
};

struct _arrayIndex{
  int *array;
  int used;
  int size;
};

/**
 * Pre:
 * Post: Retorna true si la segunda cadena comienza con el contenido de la primer cadena.
 * */
bool startsWith(const char *string1, const char *string2) {
  size_t lenString1 = strlen(string1);
  size_t lenString2 = strlen(string2);
  //Si la primer cadena es mas corta, siempre es false
  if (lenString1 < lenString2) {
    return false;
  } else {
    //Comparo
    return strncmp(string1, string2, lenString2) == 0;
  }
}

/**
 * Pre:
 * Post: Verifica que una string no contenga caracteres no validos.
 * */
bool isStringValid(char const *valoresTupla) {
  bool ok = true;
  size_t cont = strlen(valoresTupla);
  while (cont > 0 && ok) {
    if (valoresTupla[cont] == '>'
        || valoresTupla[cont] == '<'
        || valoresTupla[cont] == '!'
        || valoresTupla[cont] == '='
        || valoresTupla[cont] == '*') {
      ok = false;
    }
    cont--;
  }
  return ok;
};

/**
 * Pre:
 * Post: retorna un array de parsedData
 * */
ParsedData parseUpdateOrDelete(char const *condition) {
  ParsedData parsed = new _parsedData[3];
  char str[strlen(condition)];
  strcpy(str, condition);
  char *pch;
  //Primer sub string
  pch = strtok(str, "<=>*!");
  int pos = 0;
  while (pch != NULL) {
    //guardo el substring
    parsed[pos].data = new char[strlen(pch) + 1];
    strcpy(parsed[pos].data, pch);
    pch = strtok(NULL, "<=>*!");
    pos++;
  }
  for (int i = pos; i < 2; i++) {
    parsed[pos].data = new char[1];
    strcpy(parsed[i].data, "");
  }
  //me copio solo el operador, con mucha magia ♥
  char comparator = condition[strlen(parsed[0].data)];
  parsed[2].data = new char[2];
  parsed[2].data[0] = comparator;
  parsed[2].data[1] = '\0';
  return parsed;
}

/**
 * Pre:
 * Post: retorna un array de parsedData
 * */
ParsedData parseInsert(char const *valoresTupla, int columns) {
  ParsedData parsed = new _parsedData[columns];
  char str[strlen(valoresTupla)];
  strcpy(str, valoresTupla);
  char *pch;
  //Primer sub string
  pch = strtok(str, ":");
  int pos = 0;
  while (pch != NULL) {
    //guardo el substring
    parsed[pos].data = new char[strlen(pch) + 1];
    strcpy(parsed[pos].data, pch);
    pch = strtok(NULL, ":");
    pos++;
  }
  for (int i = pos; i < columns; i++) {
    parsed[i].data = NULL;
  }
  return parsed;
}

/**
 * Pre:
 * Post: retorna true si todos los datos parseados son diferentes de NULL
 * */
bool areAllColumnsProvided(ParsedData parsed, int length) {
  bool result = true;
  for (int i = 0; i < length; ++i) {
    result = result && parsed[i].data != NULL;
  }
  return result;
}

/**
 * pre:
 * post: retorna True si el dato en la posicion index es entero
 * */
bool isInt(ParsedData parsed, int index) {
  size_t cont = strlen(parsed[index].data);
  char *c = parsed[index].data;
  bool result = true;
  for (size_t j = 0; j < cont; ++j) {
    result = isdigit(*c) && result;
    c++;
  }
  return result;
}

/**
 * pre:
 * post: retorna el string de la posicion index
 * */
char *getParsedValue(ParsedData parsed, int index) {
  return parsed[index].data;
}

/**
 * pre:
 * post: libera toda la memoria del los datos parseados
 * */
void deleteParsedData(ParsedData parsed, int pos) {
  for (int i = 0; i < pos; ++i) {
    delete[] parsed[i].data;
  }
  delete[] parsed;
}

/**
 * Pre:
 * Post: retorna un ArrayIndex con un array de int de largo initialSize
 * */
ArrayIndex initArray(int initialSize) {
  ArrayIndex a = new _arrayIndex;
  a->array = (int *) malloc(initialSize * sizeof(int));
  a->used = 0;
  a->size = initialSize;
  return a;
}

/**
 * Pre:
 * Post: Agrega una item al array de int y aumenta su largo en 1
 * */
void insertArray(ArrayIndex a, int element) {
  if (a->used == a->size) {
    a->size++;
    a->array = (int *) realloc(a->array, a->size * sizeof(int));
  }
  a->array[a->used] = element;
  a->used++;
}

/**
 * Pre:
 * Post: Elimina la estructura
 * */
void freeArray(ArrayIndex a) {
  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
  delete a;
}

/**
 * Pre:
 * Post: Retorna hasta que posicion hay datos en el array
 * */
int getArraySize(ArrayIndex a){
  return a->used;
}

/**
 * Pre:
 * Post: Retorna el valor almacenado en un indice
 * */
int getArrayValue(ArrayIndex a, int index){
  return a->array[index];
}
