#ifndef DATA
#define DATA

#include "utils.hpp"
#include "column.hpp"

typedef struct _data *Data;

void deleteAllData(Data &data);

void addDataAtIndex(Data &data, char const *value, int index);

bool existsValue(Data dataList, char const *value);

void getDataIndex(Data listData, char const *comparator, char const *searchValueStr, _type type, ArrayIndex &a);

void deleteDataAt(Data &data, int index);

int addDataSorted(Data &data, char const *value, _type type);

void printData(Data data, int index, bool includeColon);

int getDataCount(Data data);

void updateDataAt(Data &data, int index, char const *newValue);
#endif
