#ifndef COLUMN
#define COLUMN

#include "utils.hpp"

typedef struct _column *Column;
enum _type {
  INTEGER, STRING
};

bool canPkColumnBeDeleted(Column columnList);

Column createColumn(char const *name, _type type);

Column getColumn(char const *name, Column column);

bool existsPk(Column columnList, char const *value);

bool columnExists(Column columnList, char const *columnName);

bool columnHasData(Column columnList);

bool isColumnPk(Column columnList, char const *columnName);

bool typeMatches(Column columnList, char const *targetColumn, char const *condition);

bool typesMatchColumns(Column columnList, ParsedData parsed);

void deleteAllColumns(Column &columnList);

void deleteRow(Column columnList, char const *searchCol, char const *comparator, char const *condition);

void printColumn(Column columnList);

void deleteColumn(Column &columnList, char const *columnName);

void insertRow(Column &columnList, ParsedData parsed);

void addColumn(Column &columnList, char const *columnName, _type type);

void updateRow(Column &columnList, char const *searchColumn, char const *columnToMod, char const *comparator,
               char const *searchValue, char const *newValue);

#endif
