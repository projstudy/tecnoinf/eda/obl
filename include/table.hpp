#ifndef TABLE
#define TABLE

#include "column.hpp"
#include "utils.hpp"

typedef struct _table *Table;

Table getTable(Table tableList, char const *name);

Table createTable(char const *name);

bool typeMatchesColumn(Table table, char const *targetColumn, char const *condition);

bool tableHasColumn(Table table, char const *columnName);

bool tableHasData(Table table);

bool isTablePkColumn(Table table, char const *nombreCol);

bool isTableEmpty(Table table);

bool typesMatchTable(Table table, ParsedData parsed);

bool isPkTaken(Table table, char const *pk);

int getColumnsQty(Table table);

void addTable(Table &tableList, char const *name);

void deleteTable(Table &tableList, char const *name);

void addColumnToTable(Table &table, char const *nombreCol, _type type);

void deleteColumFromTable(Table &table, char const *columnName);

void insertIntoTable(Table &table, ParsedData parsed);

void deleteTableRow(Table &table, char const *targetColumn, char const *comparator, char const *condition);

void updateTableRows(Table &table, char const *targetColumn, char const *comparator, char const *condition,
                     char const *columnaModificar, char const *valorModificar);

void printTable(Table table);
bool canTablePkColumnBeDeleted(Table table);
#endif
