#ifndef UI
#define UI
//Secuencia de escape ANSI para colores
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define BLINK "\x1B[5m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"
#include "database.hpp"
#include "column.hpp"

void getDataCreateTable();
void clearScreen();
void mainMenu(Database &db);
void getDataCreateTable(Database &db);
void getDataDropTable(Database &db);
void getDataAddColumn(Database &db);
void getDataDropColumn(Database &db);
void getDataDeleteTupla(Database &db);
void getDataInsertTupla(Database &db);
void getDataUpdate(Database &db);
void getDataPrintTable(Database &db);
void pressEnter();

#endif
