#ifndef DB
#define DB

#include "table.hpp"

enum _retorno {
  OK, ERROR, NO_IMPLEMENTADA
};
typedef enum _retorno TipoRet;
typedef struct _database *Database;

TipoRet createTable(char const *nombreTabla, Database &db, char *msg);

TipoRet dropTable(char const *nombreTabla, Database &db, char *msg);

TipoRet addCol(char const *nombreTabla, char const *nombreCol, _type type, Database &db, char *msg);

TipoRet dropCol(char const *nombreTabla, char const *nombreCol, Database &db, char *msg);

TipoRet insertInto(char const *nombreTabla, char const *valoresTupla, Database &db, char *msg);

TipoRet deleteFrom(char const *nombreTabla, char const *condicionEliminar, Database &db, char *msg);

TipoRet update(char const *nombreTabla, char const *condicionModificar,
               char const *columnaModificar, char const *valorModificar, Database &db, char *msg);

TipoRet printDataTable(char const *nombreTabla, Database db, char *msg);

Database createDB();

void dropDB(Database &db);

bool isDbEmpty(Database db);

#endif
