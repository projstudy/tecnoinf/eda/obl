#ifndef UTILS
#define UTILS
typedef struct _parsedData *ParsedData;
typedef struct _arrayIndex *ArrayIndex;

ParsedData parseInsert(char const *valoresTupla, int columns);

ParsedData parseUpdateOrDelete(char const *condition);

char *getParsedValue(ParsedData parsed, int index);

void deleteParsedData(ParsedData parsed, int pos);

bool isStringValid(char const *valoresTupla);

bool startsWith(const char *string1, const char *string2);

bool areAllColumnsProvided(ParsedData parsed, int length);

bool isInt(ParsedData parsed, int index);

ArrayIndex initArray(int initialSize);

void insertArray(ArrayIndex a, int element);

int getArraySize(ArrayIndex a);

int getArrayValue(ArrayIndex a, int index);

void freeArray(ArrayIndex a);

#endif
