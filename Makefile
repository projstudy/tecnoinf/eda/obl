all: db.cpp column.o table.o database.o data.o ui.o utils.o
	g++ -Wall -o run db.cpp column.o table.o database.o data.o ui.o utils.o

column.o: src/column.cpp include/column.hpp
	g++ -Wall -c src/column.cpp

table.o: src/table.cpp include/table.hpp
	g++ -Wall -c src/table.cpp

database.o: src/database.cpp include/database.hpp
	g++ -Wall -c src/database.cpp

data.o: src/data.cpp include/data.hpp
	g++ -Wall -c src/data.cpp

ui.o: src/ui.cpp include/ui.hpp
	g++ -Wall -c src/ui.cpp

utils.o: src/utils.cpp include/utils.hpp
	g++ -Wall -c src/utils.cpp

clean:
	rm -fr run *.o
